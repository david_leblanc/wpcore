<?php
/**
 * The default index page template
 */

get_header();

    if(have_posts()) :
        while(have_posts()) : the_post();
            the_content();
        endwhile;
    else :
        echo "No content...";
    endif;

get_footer();
?>