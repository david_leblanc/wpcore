<?php
/**
 * The template for displaying all pages.
 */

get_header();

    if(have_posts()) : 
		while(have_posts()) : the_post();
			the_content();
		endwhile;
	else :
        echo "No content...";
	endif;

get_footer(); 
?>
