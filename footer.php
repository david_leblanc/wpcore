<?php
/**
 * The file for displaying the footer.
 */
?>
		<footer>
			<?php wp_nav_menu( array( 'sort_column' => 'menu_order', 'menu_class' => 'footerMenu', 'theme_location' => 'footer-menu' ) ); ?>
		</footer>
		<?php
			wp_footer();
		?>
    </body>
</html>
