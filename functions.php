<?php
//Add support for WordPress 3.x's custom menus
add_action( 'init', 'register_my_menu' );
 
//Register area for custom menu
function register_my_menu() {
    register_nav_menu( 'main-menu', __( 'Main Menu' ));
	register_nav_menu( 'footer-menu', __( 'Footer Menu' ));
}