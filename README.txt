Theme Name: wpCore
Theme 
Author: David LeBlanc
Version: 2.0

Basic Starter theme for wordpress 3.x based on fondation.
Url : https://bitbucket.org/david_leblanc/wpcore.git

Include many featured like :
- Responsive 
- HTML5 
- CSS3
- Buttons styles
- Forms styles
- Alert boxes
- etc.

For more information about fondation go to : http://foundation.zurb.com/