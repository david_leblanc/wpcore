<?php
/**
 * The Header for our theme.
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js" <?php language_attributes(); ?>>
<!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
		<?php
			/*
			 * Print the <title> tag based on what is being viewed.
			 */
			global $page, $paged;

			wp_title( '|', true, 'right' );

			// Add the blog name.
			bloginfo( 'name' );

			// Add the blog description for the home/front page.
			$site_description = get_bloginfo( 'description', 'display' );
			if ( $site_description && ( is_home() || is_front_page() ) )
				echo " | $site_description";
			?>
		</title>
        <meta name="description" content="">

        <script src="<?php bloginfo('template_url'); ?>/js/vendor/jquery.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/vendor/custom.modernizr.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/foundation.min.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/scripts.js"></script>
		
        <link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<?php
			wp_head();
		?>
    </head>
    <body <?php body_class(); ?>>
		<nav>
			<?php wp_nav_menu( array( 'sort_column' => 'menu_order', 'menu_class' => 'mainMenu', 'theme_location' => 'main-menu' )); ?>
		</nav>